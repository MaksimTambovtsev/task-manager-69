package ru.tsc.tambovtsev.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.tambovtsev.tm.api.model.ICommand;
import ru.tsc.tambovtsev.tm.event.ConsoleEvent;
import ru.tsc.tambovtsev.tm.listener.AbstractListener;

import java.util.Collection;

@Component
public final class ArgListListener extends AbstractSystemCommandListener {

    @NotNull
    public static final String NAME = "arguments";

    @NotNull
    public static final String DESCRIPTION = "Show arguments list.";

    @NotNull
    public static final String ARGUMENT = "-arg";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    @EventListener(condition = "@argListListener.getName() == #event.name")
    public void handlerConsole(@NotNull final ConsoleEvent event) {
        System.out.println("[ARGUMENTS]");
        @NotNull final Collection<AbstractListener> commands = getArguments();
        for (final ICommand command : commands) {
            String argument = command.getArgument();
            if (argument != null && !argument.isEmpty())
                System.out.println(command.getArgument());
        }
    }

}

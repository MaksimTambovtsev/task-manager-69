package ru.tsc.tambovtsev.tm.listener.domain;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.tambovtsev.tm.dto.request.DataJsonSaveFasterXmlRequest;
import ru.tsc.tambovtsev.tm.enumerated.Role;
import ru.tsc.tambovtsev.tm.event.ConsoleEvent;

@Component
public class DomainSaveJsonFasterXMLListener extends AbstractDomainListener {

    @NotNull
    private final static String NAME = "save-json-fasterxml";

    @NotNull
    private final static String DESCRIPTION = "Save projects, tasks and users in json fasterxml";

    @Nullable
    @Override
    public String getName() {
        return NAME;
    }

    @Nullable
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @SneakyThrows
    @EventListener(condition = "@domainSaveJsonFasterXMLListener.getName() == #event.name")
    public void handlerConsole(@NotNull final ConsoleEvent event) {
        getDomainEndpoint().saveDataJsonFasterXml(new DataJsonSaveFasterXmlRequest(getToken()));
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}

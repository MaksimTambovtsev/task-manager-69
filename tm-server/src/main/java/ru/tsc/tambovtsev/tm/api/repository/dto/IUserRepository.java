package ru.tsc.tambovtsev.tm.api.repository.dto;

import org.jetbrains.annotations.Nullable;
import ru.tsc.tambovtsev.tm.dto.model.UserDTO;

public interface IUserRepository extends IRepository<UserDTO> {

    @Nullable
    UserDTO findByLogin(@Nullable String login);

    @Nullable
    UserDTO findByEmail(@Nullable String email);

    void deleteByLogin(@Nullable String login);

}

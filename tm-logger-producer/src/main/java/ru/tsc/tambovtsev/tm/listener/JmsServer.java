package ru.tsc.tambovtsev.tm.listener;

import lombok.Getter;
import lombok.SneakyThrows;
import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.broker.BrokerService;
import org.jetbrains.annotations.NotNull;

import javax.jms.*;

@Getter
public class JmsServer {

    @NotNull
    private static final String URL = ActiveMQConnection.DEFAULT_BROKER_URL;

    @NotNull
    private static final String QUEUE = "LOGGER";

    @NotNull
    private final BrokerService broker = new BrokerService();

    @NotNull
    private final ConnectionFactory connectionFactory = new ActiveMQConnectionFactory(URL);

    @NotNull
    private final Connection connection;

    @NotNull
    private final Session session;

    @NotNull
    private final Queue destination;

    @NotNull
    private final MessageProducer producer;

    @SneakyThrows
    public JmsServer() {
        broker.addConnector("tcp://localhost:61616");
        broker.start();
        connection = connectionFactory.createConnection();
        connection.start();
        session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        destination = session.createQueue(QUEUE);
        producer = session.createProducer(destination);
    }

    @SneakyThrows
    public void send (@NotNull final String text) {
        final TextMessage message = session.createTextMessage(text);
        producer.send(message);
    }

}
